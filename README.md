# Vantage QU4RTET *Amazon Machine Image* (AMI) Bundle

Vantage Solutions is proud to announce the first-of-its-kind QU4RTET
Amazon EC2 AMI bundle.  This virtual QU4RTET server image is a ready-to-deploy
production-grade implementation of the open-source QU4RTET Level-4 platform
and can be set up in minutes.

In this guide you'll find instructions for a quick setup, a more advanced setup
for production systems and an overview of the technologies the bundle utilizes.

If you require assistance getting your QU4RTET instance running,
you can [contact Vantage Solutions here](https://www.vantage-cg.com).

## Documentation, Guides and Tutorials

[The full documentation for the Vantage QU4RTET bundle is available here.](https://vantage-cg.gitlab.io/qu4rtet-ami/index.html)

## About Vantage Solutions

![Vantage](./docs/images/vantage-logo.png)

Vantage Solutions provides manufacturing consulting services to solve complex manufacturing automation issues to make your production lines run at peak efficiency and profitability. Whether it’s launching a new line, increasing productivity or complying with a mandate like serialization, Vantage Solutions has the strategic vision to design your manufacturing automation solutions and the decades of hands-on industry technical experience to build them. Our streamlined processes and stringent project management deliver outstanding results on time and on budget.

## About QU4RTET

![QU4RTET](./docs/images/QU4RTET-logo.jpg)

QU4RTET is an open-source Level-4 track-and-trace platform that enables you 
to both track your products throughout your supply chain and also meet regulatory
requirements at the same time.  QU4RTET has support for many of the GS1 supply 
chain standards around bar-coding, EPCIS, Core Business Vocabulary and trade 
item data.

QU4RTET is an open-source project maintained by SerialLab.  For more information,
see the [SerialLab Website](http://serial-lab.com) and the [QU4RTET Website](https://www.qu4rtet.com).

The source code can be found on [The SerialLab GitLab home page](https://gitlab.com/serial-lab).