# How To Get a Permanent IP Address 

Your QU4RTET server will launch with a public IP address that will
change every time your system reboots.  This is fine for a test
server or a temporary instance- but for QA and production-class
instances, you'll most certainly need a permanent IP address.

## Get an Elastic IP Address

NOTE: If you're just following along to learn, skip this section; however,
if you actually plan on using your QU4RTET server for testing, QA
or Production you'll want an IP address that doesn't change.  

1. Click *Elastic IPs* on the left nav of the [EC2 Console](https://console.aws.amazon.com/ec2/v2/)
2. On the next screen click *Allocate New Address*
![Allocate](./images/allocate-new-address.png)
3. On the next screen click *Allocate* and click *Close*.
4. On the following screen, select your instance in the dropdown
and click the *Associate* button.
![Associate](./images/associate-address.png)