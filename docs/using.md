# Connecting to and Managing Your Instance

Obviously, you'll want to be able to connect to and maintain your
instance.  There are a few ways to do this and, depending on your
preference, you may prefer one over the other:

* Amazon Systems Management
* SSH (terminal/command line)

Both can achieve the same relative functions.

## Connecting Over SSH

Amazon has a lot of material on how to connect to your instance- 
long story short is that if you are on windows and before windows 10,
you'll need an SSH client like Putty.  If you are on OSX, WIndows 10 or 
Linux you can use ssh from the command line by specifying the key you 
downloaded from AWS when you launched your QU4RTET server with the ssh -i 
flag.  For example:

    ssh -i ~/qu4rtet.pem ubuntu@18.212.33.21

If you are on Windows versions below 10,
[get putty here](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html).

## Notable QU4RTET Paths

Once you are on your server, you should know a few key paths:

|Path|Description|
|----|----|
|/srv/qu4rtet|The path to the qu4rtet source.|
|/var/log/quartet/|The QU4RTET application logs are stored here.|
|/var/log/celery/|The QU4RTET *Celery* worker logs are stored here.|
|/etc/logrotate.d/qu4rtet|The log rotation configuration file.|

## Vantage Curated Services Selection

Vantage has pre-configured the QU4RTET bundle with the following technologies.
These technologies are not the only way to implement QU4RTET- one can use
any viable web server, many different databases, etc.  This configuration
only represents Vantage's idea of a best-of-breed implementation.

|Service|Product|Description|URL|
|-------|-------|-----------|---|
|HTTP(S) Reverse Proxy|Nginx|The open source Nginx reverse proxy server handles all direct HTTP and HTTPS connectivity|[Nginx](https://nginx.com)|
|WSGI Server|Gunicorn|Gunicorn, an open source WSGI server, serves as the Python WSGI HTTP server.|[Gunicorn](http://gunicorn.org)|
|Distributed/Asynchronous Task Processing|Celery|Celery handles all QU4RTET task processing|[Celery Distributed Task Queue](http://celeryproject.com)
|Task Monitoring|Celery Flower|Flower is a web based tool for monitoring and administrating Celery clusters.  It is pre-installed and configured.|[Flower](http://flower.readthedocs.io/)
|Message Broker|RabbitMQ|RabbitMQ is the central task broker for the QU4RTET Celery workers|[RabbitMQ](https://www.rabbitmq.com/)

### Accessing Celery Flower

Celery flower access is enabled by default on port 5555.  You will need
to configure the *Security Group* associated with your EC2 instance to
allow traffic to port 5555 for the IP addresses that require access before
this will work.

To access it, for example, go to:

    #example
    https://myquartetserver.example:5555

Celery Flower lets you monitor all of your detailed tasks.

## Vantage Enabled QU4RTET Commands and Utilities

**NOTE**: All of these commands can also be executed from the
[Systems Manager](https://console.aws.amazon.com/systems-manager/)
by running remote shell script commands once that is configured in later steps.

### Restarting

To restart QU4RTET and all of its services issue the following command from
the terminal:

    restart-quartet

### Stopping

To stop all of the QU4RTET services, issue the following command from the
terminal (or the [Systems Manager](https://console.aws.amazon.com/systems-manager/).)

    stop-quartet

To start a stopped QU4RTET instance, just issue the `restart-quartet` command.

### Updating

To update QU4RTET to the latest version, issue the following command

    update-quartet

### Edit Log Rotation

By default, log rotation is configured and turned on for QU4RTET and 
*Celery* worker logs.  The configuration for rotation can be found here:

    /etc/logrotate.d/qu4rtet
