# Available Parameter Store Values
Your Vantage QU4RTET bundle is pre-configured to look in the amazon AWS *Parameter Store*
for some configuration values, such as database URLs, passwords, etc.  You can configure
your QU4RTET instance to use RDS and S3 right from your browser, for example, without ever having to log into
the QU4RTET instance.

## Usage
To use parameters in the AWS Parameter Store to configure your QU4RTET instance,
you'll need to follow the sections [Add PARAMETER_GROUP to *User Data*](production.md#param_group) and
the [AWS Systems Manager Configuration](production.md#sysman) sections.

> **IMPORTANT** If your instance is not configured with IAM to be a managed instance, and you
have not set the the `PARAMETER_GROUP` value in your EC2 instance's *User Data* then the 
values in the parameter store will not be available to QU4RTET.  

[More on EC2 Managed Instances](https://docs.aws.amazon.com/systems-manager/latest/userguide/managed_instances.html)
[More on the EC2 Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html)


## Available Parameters

|Parameter Name|Description|Type|
|--------------|-----------|----|
|USE_AWS|Default is False, set this to True to use S3 for storage.|Boolean (True/False)|
|DJANGO_AWS_ACCESS_KEY_ID|The AWS access key (see the S3 storage section below.|String|
|DJANGO_AWS_SECRET_ACCESS_KEY|The AWS secret access key (see the S3 storage section below.|String|
|DJANGO_AWS_STORAGE_BUCKET_NAME|The name of the S3 bucket created to hold QU4RTET storage (see the S3 storage section below).|String|
|DATABASE_HOST|The host URL or IP address of the database. This is typically the address of an RDS cluster server/instance|String|
|POSTGRES_DB|The name of the database to connect to- typically qu4rtet.|String|
|POSTGRES_USER|The user to connect to the database with.|String|
|POSTGRES_PORT|The port of the database. Default is 5432.|String|
|POSTGRES_PASSWORD|The password to use when connecting to the database.|SecureString|
|DJANGO_SECRET_KEY|The secret key Django uses to encrypt sensitive data.  Make sure to change this value.|SecureString|
|DJANGO_ALLOWED_HOSTS|See the Django documentation on the ALLOWED_HOSTS setting.|String|
|DJANGO_DEBUG|See the Django documentation on the DEBUG setting|String|
|DJANGO_MEDIA_URL|See the Django documentation on the MEDIA_URL setting|String|
|DJANGO_MAILGUN_API_KEY|If you'd like to use [MailGun](https://app.mailgun.com/) for email.|SecureString|
|DJANGO_SERVER_EMAIL|The email address the server will use when sending emails.|String|
|MAILGUN_SENDER_DOMAIN|The sender domain you've configured for [MailGun](https://app.mailgun.com/)|String|
|USE_SENTRY|If you'd like to use [Sentry](https://sentry.io/welcome/) logging/monitoring.  Set this to True|String|
|DJANGO_SENTRY_DSN|Put your [Sentry](https://sentry.io/welcome/) DSN here.|SecureString|
LOGGING_PATH|You can customize where your logging files go for QU4RTET's Django logs. Default is /var/log/qu4rtet|String|
LOGGING_LEVEL|You can set logging to INFO, WARNING, DEBUG or ERROR. Default is WARNING|String|
HTTPS_ONLY|If you'd like to reject any traffic coming over HTTP (80), set this to True.|String|
AUDIT_TRAIL|Set to True to enable model auditing.  Warning: this will require a database migration if this is being set up for the first time.|Boolean| 

![example](./images/example.png)