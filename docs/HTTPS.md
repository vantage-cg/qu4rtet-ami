# How To Enable HTTPS (Optional)

The default security for the base image is not configured
for HTTPS, since we (obviously) don't know who you are and 
can not possibly have a certificate ready for you ahead of time.
You'll need to get your own certificate and configure
NGINX for secure communications by following the instructions 
below.

If you are already familiar with NGINX config, feel free
to ignore this section and set up HTTPS using your own 
certificate authority and tools, etc.

## Create an A record for your server

If you want to name your server test.mycompany.com, you'll
need to create an **A Record** in your mycompany.com DNS server
that points to the IP address of your QU4RTET instance.  Once 
you have that you are a few commands away from enabling HTTPS:

## Change the ServerName in the Nginx config file

    sudo nano /etc/nginx/sites-enabled/qu4rtet

Then un-comment the `server_name` line (remove the #) and enter in 
the host name of your server that you want to associate your
certificate with, for example *test.mycompany.org*.  Save the file
and exit and then restart nginx:

    sudo service nginx restart

## Run the Certbot Command

Note: this will create a *Let's Encrypt* valid HTTPS certificate,
install it and add it to your QU4RTET instance.  Make sure you have
your A Record in your DNS working and also make sure port 80 is open
for traffic in your AWS security group for your QU4RTET server.

    cd /srv/qu4rtet/ && . utility/enable-https.sh

Then follow the certbot instructions and you'll be good to go
with your new, secure QU4RTET application server.