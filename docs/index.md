# The Vantage QU4RTET AWS AMI Bundle

Vantage Solutions is proud to announce the first-of-its-kind QU4RTET
Amazon EC2 AMI bundle.  This virtual QU4RTET server image is a ready-to-deploy
production-grade implementation of the open-source QU4RTET Level-4 platform
and can be set up in minutes.

In this documentation you'll find instructions for a quick setup, a more advanced setup
for production systems and an overview of the technologies the bundle utilizes.

## About Vantage Solutions

![Vantage](./images/vantage-logo.png)

Vantage Solutions provides manufacturing consulting services to solve complex manufacturing automation issues to make your production lines run at peak efficiency and profitability. Whether it’s launching a new line, increasing productivity or complying with a mandate like serialization, Vantage Solutions has the strategic vision to design your manufacturing automation solutions and the decades of hands-on industry technical experience to build them. Our streamlined processes and stringent project management deliver outstanding results on time and on budget.

### Need Help or Support?

If you need help with this tutorial or have technical questions, feel free to reach out:
[Vantage Support](mailto:support.services@vantage-cg.com)

### Contact Vantage

If you'd like to engage with Vantage beyond this tutorial, we offer Validation, Managed Systems Hosting,
Installation Services, Custom Integration Software Development and a wide range of
serialization and supply-chain services for manufacturing up through distribution. We have
offices in the US, Latin America and the EU. [Vantage Contact Information](https://www.vantage-cg.com/contact-us/).

## About QU4RTET

![QU4RTET](./images/QU4RTET-logo.jpg)

QU4RTET is a modern, modular, open-source Level-4 track-and-trace platform that enables you 
to both track your products throughout your supply chain and also meet regulatory
requirements at the same time.  QU4RTET has support for many of the GS1 supply 
chain standards around bar-coding, EPCIS, Core Business Vocabulary and trade 
item data.

QU4RTET is an open-source project maintained by SerialLab.  It is free to use and there are 
no license fees.  It is distributed under the GNU GPLv3 and GNU AGPLv3 licenses. For more information,
see the [SerialLab Website](http://serial-lab.com) and the [QU4RTET Website](https://www.qu4rtet.com).

The source code can be found on [The SerialLab GitLab home page](https://gitlab.com/serial-lab).