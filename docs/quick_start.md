# Quick Start

We will have your QU4RTET instance up, running
and communicating in a matter of minutes- just follow along!  If you want
to get a production-ready instance up as well, follow here and then move on
to the [Setting Up a Production Instance](production.md) tutorial.

> **Note**: The base AMI is fine for test, sandbox and other types of uses; however,
it is not suitable for commercial use or any public deployment where ti can be accessed
by a broad range of IP addresses until it is secured.  See the [How To Enable HTTPS](HTTPS.md)
guide for an example of how to encrypt all traffic to and from the instance.  This part
of the tutorial will not cover this.

## TL;DR

If you are already familiar with EC2 and AWS- we'll make this real easy.
Goto your EC2 home page, go to the AMIs menu item, search for QU4RTET
under *Public Images*, select QU4RTET and click the Launch button or [click here](https://aws.amazon.com/marketplace/pp/B07GTBJ4GM).  Then
[go here](https://gitlab.com/serial-lab/quartet-ui/) to get the QU4RTET UI
download link and [go here](https://serial-lab.gitlab.io/quartet-ui/add-server/)
to learn how to connect to your QU4RTET instance.

![tldr](./images/tldr.png)

## Get an Amazon Web Services Account

You will need an Amazon EC2 account to follow along with this
tutorial.  It is free to join, only requires a credit card to be on file
and you can sign up here:

[Amazon AWS](https://aws.amazon.com/)

## Launch a Vantage QU4RTET AMI

Once you are signed up, setting up a QU4RTET instance is a few
clicks away.

### Find and Subscribe

1. Next we will launch the Vantage QU4RTET AMI.  You can find that AMI 
    and its info here: [QU4RTET on Amazon AWS Marketplace](https://aws.amazon.com/marketplace/pp/B07GTBJ4GM)

2. Click on the *Vantage QU4RTET Bundle* Link.

3. Click on the *Continue to Subscribe* button.

4. Click on the *Accept Terms* button on the next page.

### Configure

Your page may take a few seconds to refresh. When the page refreshes, click the *Continue To Configuration* button.

On the following page you'll be asked to provide a few values.  The first two, *Fulfillment Option* and 
*Software Version* you can leave for now (at the time of this writing there is only one version available- you can
check to see if there are newer versions and launch them if you wish.).  The third value specifies what region
of the world you'd like your QU4RTET instance to be launched in.  In general, best practices suggest that 
the instance should be closest to its most active data-sources.  Select a region that is either close to you
or close to the data-sources (manufacturing plants, packaging facilities, etc.)  If your data is scattered about,
try to select a centralized *Region*.
![Configure](./images/Configure.png)

Next, click the *Continue to Launch* button.  On the next page, select either the *Launch from Website* button, or (if you are already comfortable in AWS), select the *Launch in EC2* option.  
![Launch Options](./images/Launch.png)

#### EC2 Instance Type
This is an important choice, if you are setting up a production instance you should be aware of your
sizing requirements and making sure that you're aware of other options you have- such as load balancing,
distributed on-demand computing, distributed task and brokers for high-volume messaging, etc.  If you
need something along those lines and are uncomfortable setting it up [contact Vantage](https://www.vantage-cg.com/contact-us/)

Generally speaking, any of the medium and large instance types are good for test and QA servers.  If you're
just curious about setting up QU4RTET- select a small or even nano size server.  Just don't expect it to
perform well with large work-loads.

> **Note**: If you are launching a self-contained QA, Test or sandbox server you can ignore the *VPC Settings*
and *Subnet Settings* dropdowns.  If you do intend to move on to the [Setting Up a Production Instance](production.md) tutorial,
click *Create a VPC in EC2* and create a VPC (Virtual Private Cloud), and then create a subnet on that VPC by clicking
the *Create a subnet in EC2* link.  If at anytime, you feel like this is overwhelming or too technical feel free
to reach out to [Vantage Support](mailto:support.services@vantage-cg.com).  We can help you set up a production instance fairly quickly.  

Under the *Security Group Settings*, you can accept the default if you are new to AWS or you
can create a new *Security Group*.  The security group in AWS allows you to specify which IP addresses
will have access to your system over what ports once the system is launched.  If you restrict access
to port 80, just be aware that some people may not be able to connect to the instance.  **If you are
planning on launching a production instance- you should certainly know what you are are doing here
and restrict access according to your business scenario.**

Under the *Key Pair Settings*, if you don't have a Key Pair already, click the *Create a key pair in EC2*. This
will take you to a page that allows you to create a secure Key Pair that will allow you to access you
QU4RTET system directly from the command line via SSH.  You shouldn't need this very often- or at all; however,
**it is important that you do not lose this key once you create and download it.**  In addition, since
the key allows access to your system's terminal/command line **it must be kept secure**.

If you need to, click the *Create a key pair in EC2* and on the following page click the
*Create Key Pair* button, give your keypair a name and save the resulting file **somewhere safe**
(where you will not lose it) and **secure** (where unauthorized individuals may access it.)

![key](./images/KeyPair.png)

### Launch

You're ready to launch.  Go ahead and click the launch button and your QU4RTET instance
will be up shortly.

![Launch](./images/LaunchButton.png)

You should see a confirmation that looks something like this:

![congrats](./images/congrats.png)

**Important**: Click on the *EC2 Console* link and follow the instructions below:

## Get QU4RTET-UI and Connect

Now that your QU4RTET platform is up, you'll need to get the **Public IP Address** of the instance.

### Get the Public IP

In the EC2 page, click on your QU4RTET instance. On your instance's details page, you'll see a number of things at the bottom under the *Description* Tab.  Locate the **IPv4 Public IP** field and note, copy or write down the value there.
**Do not use the Private IPs field**.

![public ip](./images/publicip.png)

Now that you have the instance up and your public IP address, you're ready to download
QU4RTET UI and connect!  

If you'd like a permanent (elastic) public IP (the one you are given initially will change during reboots),
follow the instructions here:

Quick guide:
[Get a Permanent IP Address](permanent_ip.md)

Amazon's Documentation:
[Amazon Elastic IP Addresses](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html)

### Get QU4RTET UI

[Get QU4RTET UI binary download and/or source code here.](https://gitlab.com/serial-lab/quartet-ui/)

### Connect to Your QU4RTET Instance

The default user credentials for the QU4RTET AMI can be found on the
[Default User](default_user.md) page.  You will need this to initially connect.
Follow the best practices suggestion on the *Default User* page to help
secure your system further.

[Instructions for Connecting to QU4RTET are here.](https://serial-lab.gitlab.io/quartet-ui/add-server/)