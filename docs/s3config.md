# Configuring Amazon S3

Your Vantage QU4RTET instance is pre-configured to store task data
(inbound messages) and log files in Amazon S3 if you wish.  All you have
to do is enable AWS systems manager ([See the Parameters Guide](parameters.md)),
set up a bucket and configure a few simple parameter settings in 
the AWS Parameter Store.

## Create The Bucket in S3

Go to the AWS Console and Click on the S3 link. Then 
click on the *Create Bucket* button.  

Create bucket named *qu4rtet-tasks* by using a DNS compliant name
for example:

    serial-lab.com.quartet-tasks

> Obviously, don't use `serial-lab.com`...use your organizations domain or something
that you know will be unique.

Accept all the defaults after creating the bucket **do not make your bucket public**.

## Create a User To Access the Bucket

Go to the [Amazon Identity and Access Management Page](https://console.aws.amazon.com/iam/home#/home)
and create a new user by clicking *Users* then *Add User* on the following page.  

On the new user page, provide a name and then **make sure to select
the *Programmatic access* check box**.

![add user](./images/add_user.png)

On the next page, Click attach existing policies directly and search
for S3.  Select *AmazonS3FullAccess* and click the *Next: Review* button
at the bottom of the page.

![policies](./images/policies.png)

Review the user you created and click the *Create user* button on the
next page. 

Take note to download the .csv file with the access keys!  Click on the
*Download.csv* file and save it somewhere **safe and secure**.

## Add S3 Parameters to the Parameter Store

> Note: To use parameters in the AWS Parameter Store to configure your QU4RTET instance,
you'll need to follow the sections [Add PARAMETER_GROUP to *User Data*](production.md#param_group) and
the [AWS Systems Manager Configuration](production.md#sysman) sections.

### Things you will need:

* The name of the S3 bucket we created above.
* Your .csv file from the IAM steps above.
* The name of your PARAMETER_GROUP.  For example, if the *User Data* on your EC2 QU4RTET instance
is PARAMETER_GROUP='GROUP-1' then your group name is GROUP-1.

### Enter the Parameters

Go to the [EC2 home page](https://console.aws.amazon.com/ec2/v2/home) and at the bottom left menu under *Systems Manager Shared Resources* select the *Parameter Store* link.

![param store](./images/param-store.png)

QU4RTET parameters are entered into the parameter store in the following naming format:

    /[group name]/[parameter name]
    # for example
    /GROUP-1/USE_AWS/

Enter in the following parameters (assumes your group name is 'GROUP-1'):

|Parameter Name|Value|Type|Description
|--------------|-----------|----|-----|
|/GROUP-1/USE_AWS|True|String|This tells your QU4RTET instance to load AWS configurations.|
|/GROUP-1/DJANGO_AWS_ACCESS_KEY_ID|Get this value from your .csv spread sheet.|String|This is how your QU4RTET instance identifies itself to AWS to get access to the S3 bucket we created.|
|/GROUP-1/DJANGO_AWS_SECRET_ACCESS_KEY|Get this value from your .csv|SecureString|This is basically the password for your S3 access.|
|/GROUP-1/DJANGO_AWS_STORAGE_BUCKET_NAME|This is the name of the bucket you created above.|String|The name of the bucket that QU4RTET will store it's data in.|

### Reboot Your Server

Go to the [EC2 home page](https://console.aws.amazon.com/ec2/v2/home), select your running
QU4RTET instance, right click *Instance State/Reboot*

![reboot](./images/reboot.png)